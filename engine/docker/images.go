package docker

import (
	"context"
	"io"
	"os"
	"strings"

	"github.com/docker/docker/api/types"
)

func GetImagesList() ([]string, error) {
	cli, err := newCli()
	if err != nil {
		return nil, err
	}

	imgs, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	if err != nil {
		return nil, err
	}

	var ret []string
	for _, img := range imgs {
		ret = append(ret, img.RepoTags...)
	}

	return ret, nil
}

func normalizeImageName(image string) string {
	if !strings.Contains(image, ":") {
		image += ":latest"
	}

	return image
}

func HasImage(image string) (bool, error) {
	cli, err := newCli()
	if err != nil {
		return false, err
	}

	image = normalizeImageName(image)

	imgs, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	if err != nil {
		return false, err
	}

	for _, img := range imgs {
		for _, name := range img.RepoTags {
			if name == image {
				return true, nil
			}
		}
	}

	return false, nil
}

func PullImage(image string) error {
	cli, err := newCli()
	if err != nil {
		return err
	}

	image = normalizeImageName(image)

	reader, err := cli.ImagePull(context.Background(), image, types.ImagePullOptions{})
	if err != nil {
		return err
	}
	defer reader.Close()

	_, err = io.Copy(os.Stdout, reader)
	return err
}
