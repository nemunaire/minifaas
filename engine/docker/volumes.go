package docker

import (
	"context"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/mount"
)

func CreateVolumeDir(target string, readOnly bool) (*mount.Mount, error) {
	abs, err := filepath.Abs("artifacts")
	if err != nil {
		return nil, err
	}

	dir, err := ioutil.TempDir(abs, "")
	if err != nil {
		return nil, err
	}

	return &mount.Mount{
		Type:     mount.TypeBind,
		Source:   dir,
		Target:   target,
		ReadOnly: readOnly,
	}, nil
}

func GetArtifactsVolumes(id string) (ret []string, err error) {
	abs, err := filepath.Abs("artifacts")
	if err != nil {
		return nil, err
	}

	mnt, err := GetVolumes(id)
	if err != nil {
		return nil, err
	}

	for _, m := range mnt {
		if m.Type == mount.TypeBind && strings.HasPrefix(m.Source, abs) {
			ret = append(ret, strings.TrimPrefix(m.Source, abs))
		}
	}

	return
}

func GetVolumes(id string) (ret []types.MountPoint, err error) {
	cli, err := newCli()
	if err != nil {
		return nil, err
	}

	ctr, err := cli.ContainerInspect(context.Background(), id)
	if err != nil {
		return nil, err
	}

	for _, mnt := range ctr.Mounts {
		ret = append(ret, mnt)
	}

	return
}
