package docker

import (
	"crypto/rand"
	"fmt"

	"github.com/nemunaire/minifaas/engine"
)

func genContainerName(jobtype string) string {
	uuid := make([]byte, 24)
	_, err := rand.Read(uuid)
	if err != nil {
		panic(err.Error())
	}

	return fmt.Sprintf("%s%x", engine.GenContainerPrefix(jobtype), uuid)
}
