package engine

import (
	"crypto/sha256"
	"fmt"
	"log"
	"strings"

	"github.com/nemunaire/minifaas/jobs"
)

const CTR_NAME_PREFIX = "minifaas"

func GenContainerPrefix(jobtype string) string {
	return fmt.Sprintf("%s-%x-", CTR_NAME_PREFIX, sha256.Sum224([]byte(jobtype)))
}

func ParseContainerName(name string) (jobtype, id string, err error) {
	if !strings.HasPrefix(strings.TrimPrefix(name, "/"), CTR_NAME_PREFIX+"-") {
		return "", "", fmt.Errorf("This is not a %s job: starting with %q", CTR_NAME_PREFIX, name)
	}

	tmp := strings.Split(name, "-")
	if len(tmp) < 3 {
		return "", "", fmt.Errorf("This is not a %s job: %q didn't has at least 3 args", CTR_NAME_PREFIX, name)
	}

	jobtype = jobs.GetJobType(tmp[1])
	if jobtype == "" {
		return "", "", fmt.Errorf("This is not a %s job: unknown job type %q", CTR_NAME_PREFIX, tmp[1])
	}

	id = strings.Join(tmp[2:], "-")

	return
}

func FilterRunningContainers(jobtype string, ctrs map[string]string) (ret []string) {
	prefix := GenContainerPrefix(jobtype)

	for cname, _ := range ctrs {
		if jobtype == "" || strings.HasPrefix(cname, prefix) {
			ret = append(ret, cname)
		}
	}

	return
}

func CountRunningContainers(jobtype string, ctrs map[string]string) (n int) {
	prefix := GenContainerPrefix(jobtype)

	log.Println(ctrs)

	for cname, _ := range ctrs {
		log.Println(strings.TrimPrefix(cname, "/"), prefix)
		if jobtype == "" || strings.HasPrefix(strings.TrimPrefix(cname, "/"), prefix) {
			n += 1
		}
	}

	return
}
