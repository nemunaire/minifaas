package config

import (
	"flag"
)

// declareFlags registers flags for the structure Options.
func (c *Config) declareFlags() {
	flag.BoolVar(&c.Dev, "dev", c.Dev, "Use ui directory instead of embedded assets")
	flag.StringVar(&c.Bind, "bind", c.Bind, "Bind port/socket")
	flag.StringVar(&c.ExternalURL, "externalurl", c.ExternalURL, "Begining of the URL, before the base, that should be used eg. in mails")

	// Others flags are declared in some other files when they need specials configurations
}

func Consolidated() (cfg *Config, err error) {
	// Define defaults options
	cfg = &Config{
		Bind:        ":8082",
		ExternalURL: "http://localhost:8082",
	}

	cfg.declareFlags()

	// Then, overwrite that by what is present in the environment
	err = cfg.FromEnv()
	if err != nil {
		return
	}

	// Finaly, command line takes precedence
	err = cfg.parseCLI()
	if err != nil {
		return
	}

	return
}

// parseCLI parse the flags and treats extra args as configuration filename.
func (c *Config) parseCLI() error {
	flag.Parse()

	return nil
}
