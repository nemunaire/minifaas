// +build !dev

package ui

import (
	"embed"
	"net/http"
)

//go:embed *

var _assets embed.FS

var Assets http.FileSystem

func init() {
	Assets = http.FS(_assets)
}
