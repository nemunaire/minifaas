function getVersion() {
  fetch('/api/version')
    .then(function(response) {
      return response.json();
    })
    .then(function(version) {
      document.getElementById("version").innerHTML = "v" + version.version;
    });
}

let current_job = null;

function runctnr() {
  document.getElementById("btnlaunch").disabled = true;
  document.getElementById("btnlaunchspinner").style.display = "inline-block";
  document.getElementById("error-alert").style.display = "none";

  fetch('/api/run')
    .then(function(response) {
      if (response.status >= 200 && response.status <= 299) {
        return response.json();
      } else {
        throw response;
      }
    })
    .then(function(data) {
      current_job = data.jobid;

      document.getElementById("jobid").textContent = current_job;
      document.getElementById("started-alert").style.display = "block";

      fetchLogs();
    })
    .catch(function(error) {
      error.json().then(json => {
        document.getElementById("errortxt").textContent = json.error;
        document.getElementById("error-alert").style.display = "block";
      })

      document.getElementById("btnlaunch").disabled = false;
      document.getElementById("btnlaunchspinner").style.display = "none";
    })
  return false;
}

function fetchLogs() {
  var ws = new WebSocket('ws://' + window.location.host + '/api/jobs/' + current_job + '/logs');

  ws.onopen = function (evt) {
    console.log("Connection open ...");
    document.getElementById("logs-card").style.display = "block";
    document.getElementById("logs").innerText = "";
  };

  ws.onmessage = function (evt) {
    console.log("Received Message: " + evt.data);
    document.getElementById("logs").innerText += evt.data;
  };

  ws.onclose = function (evt) {
    console.log("Connection closed.");

    fetchVolumes()

    current_job = null;

    document.getElementById("btnlaunch").disabled = false;
    document.getElementById("btnlaunchspinner").style.display = "none";
    document.getElementById("started-alert").style.display = "none";
  };
}

function fetchVolumes() {
  fetch('/api/jobs/' + current_job + '/volumes')
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      document.getElementById("volumes").innerHTML = "";

      for (const i in data) {
        let a = document.createElement("a");
        a.className = "list-group-item list-group-item-action";
        a.href = "/artifacts" + data[i];
        a.innerText = data[i];
        document.getElementById("volumes").appendChild(a);
      }

      if (data.length > 0) {
        document.getElementById("volumes-card").style.display = "block";
      }
    })
}
