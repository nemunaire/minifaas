package ui

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/nemunaire/minifaas/config"
)

func DeclareRoutes(router *gin.Engine, cfg *config.Config) {
	if cfg.Dev {
		Assets = http.Dir("./ui")
	}

	router.GET("/", serveOrReverse("/"))
	router.GET("/favicon.ico", serveOrReverse("/favicon.ico"))
	router.GET("/manifest.json", serveOrReverse("/manifest.json"))
	router.GET("/css/*path", serveOrReverse(""))
	router.GET("/fonts/*path", serveOrReverse(""))
	router.GET("/img/*path", serveOrReverse(""))
	router.GET("/js/*path", serveOrReverse(""))
}

func serveOrReverse(forced_url string) gin.HandlerFunc {
	if forced_url != "" {
		return func(c *gin.Context) {
			c.FileFromFS(forced_url, Assets)
		}
	} else {
		return func(c *gin.Context) {
			c.FileFromFS(c.Request.URL.Path, Assets)
		}
	}
}
