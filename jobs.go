package main

import (
	"github.com/docker/docker/api/types/mount"

	"github.com/nemunaire/minifaas/engine/docker"
	"github.com/nemunaire/minifaas/jobs"
)

func RunJob(jobtype string) (string, error) {
	job := jobs.GetJob(jobtype)

	var mnts []mount.Mount
	if job.DataMount {
		myVolume, err := docker.CreateVolumeDir("/data", false)
		if err != nil {
			return "", err
		}
		mnts = append(mnts, *myVolume)
	}

	// Check if the image is here
	hasimg, err := docker.HasImage(job.Image)
	if err != nil {
		return "", err
	}
	if !hasimg {
		err = docker.PullImage(job.Image)
		if err != nil {
			return "", err
		}
	}

	return docker.Create(
		jobtype,
		job.Image,
		job.Cmd,
		mnts,
	)
}
