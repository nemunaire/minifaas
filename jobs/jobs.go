package jobs

import (
	"crypto/sha256"
	"fmt"
)

type Job struct {
	Image     string
	Cmd       []string
	DataMount bool
}

var jobs = map[string]Job{
	"counter": {
		Image:     "alpine",
		Cmd:       []string{"sh", "-c", "touch /data/work_done; for i in `seq 10`; do echo $i; sleep 0.5; done"},
		DataMount: true,
	},
}

var revJobs = map[string]string{}

func init() {
	for jobtype := range jobs {
		revJobs[fmt.Sprintf("%x", sha256.Sum224([]byte(jobtype)))] = jobtype
	}
}

func GetJobType(hashJobType string) (jobtype string) {
	var ok bool
	if jobtype, ok = revJobs[hashJobType]; !ok {
		jobtype = ""
	}

	return
}

func GetJob(jobtype string) (job Job) {
	return jobs[jobtype]
}
