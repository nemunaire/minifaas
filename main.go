package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/nemunaire/minifaas/config"
)

func main() {
	cfg, err := config.Consolidated()
	if err != nil {
		log.Fatal("Unable to read configuration:", err)
	}

	a := NewApp(cfg)
	go a.Start()

	go runQueue()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	log.Println("Stopping the service...")
	a.Stop()
	log.Println("Stopped")
}
